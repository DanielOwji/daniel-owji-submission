
create sequence tag_id_seq
  as integer
  maxvalue 2147483647;

create table if not exists "fairfax-media".article
(
  id    integer not null
    constraint article_pkey
    primary key,
  title varchar,
  body  varchar,
  date  date
);

create unique index if not exists article_id_uindex
  on "fairfax-media".article (id);

create table if not exists "fairfax-media".tag
(
  id  serial  not null
    constraint tag_id_pk
    primary key,
  tag varchar not null
);

create unique index if not exists tag_id_uindex
  on "fairfax-media".tag (id);

create unique index if not exists tag_tag_uindex
  on "fairfax-media".tag (tag);

create table if not exists "fairfax-media".article_tag_associations
(
  articleid integer not null
    constraint article_tag_associations_article_id_fk
    references "fairfax-media".article,
  tagid     integer not null
    constraint article_tag_associations_tag_id_fk
    references "fairfax-media".tag,
  constraint article_tag_associations_articleid_tagid_pk
  primary key (articleid, tagid)
);

