package main

import (
	"database/sql"
	httpDeliver "fairfax-media-technical-test/article/controller/http"
	articleRepo "fairfax-media-technical-test/article/repository"
	articleUseCase "fairfax-media-technical-test/article/usecase"
	middleware "fairfax-media-technical-test/middleware"
	"log"
	"os"

	"fmt"
	"time"

	"github.com/labstack/echo"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigFile(`config.json`)
	err := viper.ReadInConfig()

	if err != nil {
		panic(err)
	}

	if viper.GetBool(`debug`) {
		fmt.Println("Service RUN on DEBUG mode")
	}

}

func main() {

	dbHost := viper.GetString(`database.host`)
	dbPort := viper.GetString(`database.port`)
	dbUser := viper.GetString(`database.user`)
	dbPass := viper.GetString(`database.pass`)
	dbName := viper.GetString(`database.name`)
	connection := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable host=%s port=%s",
		dbUser, dbPass, dbName, dbHost, dbPort)
	dbConn, err := sql.Open("postgres", connection)
	if err != nil && viper.GetBool("debug") {
		fmt.Println(err)
	}
	err = dbConn.Ping()
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	defer dbConn.Close()
	e := echo.New()
	middL := middleware.InitMiddleware()
	e.Use(middL.CORS)
	ar := articleRepo.PostgresArticleRepository(dbConn)

	timeoutContext := time.Duration(viper.GetInt("context.timeout")) * time.Second
	au := articleUseCase.NewArticleUsecase(ar, timeoutContext)
	httpDeliver.ArticleHttpHandler(e, au)

	e.Start(viper.GetString("server.address"))
}
