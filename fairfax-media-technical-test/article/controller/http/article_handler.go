package http

import (
	"context"
	"fairfax-media-technical-test/models"
	"net/http"
	"strconv"

	articleUseCase "fairfax-media-technical-test/article"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	validator "gopkg.in/go-playground/validator.v9"
)

type ResponseError struct {
	Message string `json:"message"`
}

type HttpArticleHandler struct {
	ArticleUseCase articleUseCase.ArticleUseCase
}

func ArticleHttpHandler(e *echo.Echo, us articleUseCase.ArticleUseCase) {
	handler := &HttpArticleHandler{
		ArticleUseCase: us,
	}
	e.GET("/article/:id", handler.GetByID)
	e.POST("/articles", handler.SaveArticle)
	e.GET("/tags/:tagname/:date", handler.GetTagAnalysisByDate)
}

func (a *HttpArticleHandler) GetByID(c echo.Context) error {

	idP, err := strconv.Atoi(c.Param("id"))
	id := int64(idP)

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	art, err := a.ArticleUseCase.GetByID(ctx, id)

	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}
	return c.JSON(http.StatusOK, art)
}

func (a *HttpArticleHandler) SaveArticle(c echo.Context) error {
	var article models.Article
	err := c.Bind(&article)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	if ok, err := isRequestValid(&article); !ok {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	ar, err := a.ArticleUseCase.SaveArticle(ctx, &article)

	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}
	return c.JSON(http.StatusCreated, ar)
}

func (a *HttpArticleHandler) GetTagAnalysisByDate(c echo.Context) error {

	tagName := c.Param("tagname")
	date := c.Param("date")

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	art, err := a.ArticleUseCase.GetTagAnalysisByDate(ctx, tagName, date)

	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}
	return c.JSON(http.StatusOK, art)
}

func isRequestValid(m *models.Article) (bool, error) {

	validate := validator.New()

	err := validate.Struct(m)
	if err != nil {
		return false, err
	}
	return true, nil
}

func getStatusCode(err error) int {

	if err == nil {
		return http.StatusOK
	}

	logrus.Error(err)
	switch err {
	case models.INTERNAL_SERVER_ERROR:

		return http.StatusInternalServerError
	case models.NOT_FOUND_ERROR:
		return http.StatusNotFound
	case models.CONFLIT_ERROR:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}
