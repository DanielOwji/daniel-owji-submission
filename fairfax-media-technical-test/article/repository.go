package article

import (
	"context"
	"fairfax-media-technical-test/models"
)

type ArticleRepository interface {
	GetByID(ctx context.Context, id int64) (*models.Article, error)
	GetArticleByTitle(ctx context.Context, title string) (*models.Article, error)
	SaveArticle(ctx context.Context, a *models.Article) (int64, error)
	GetTagAnalysisByDate(ctx context.Context, tagName string, date string) (*models.TagAnalysis, error)
}
