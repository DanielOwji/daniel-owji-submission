package usecase

import (
	"context"
	"fairfax-media-technical-test/article"
	"fairfax-media-technical-test/models"
	"time"
)

type articleUseCase struct {
	articleRepos   article.ArticleRepository
	contextTimeout time.Duration
}

func NewArticleUsecase(a article.ArticleRepository, timeout time.Duration) article.ArticleUseCase {
	return &articleUseCase{
		articleRepos:   a,
		contextTimeout: timeout,
	}
}

func (a *articleUseCase) GetByID(c context.Context, id int64) (*models.Article, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	res, err := a.articleRepos.GetByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (a *articleUseCase) SaveArticle(c context.Context, m *models.Article) (*models.Article, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	articleExist, _ := a.GetArticleByTitle(ctx, m.Title)
	if articleExist != nil {
		return nil, models.CONFLIT_ERROR
	}

	id, err := a.articleRepos.SaveArticle(ctx, m)
	if err != nil {
		return nil, err
	}

	m.ID = id
	return m, nil
}

func (a *articleUseCase) GetArticleByTitle(c context.Context, title string) (*models.Article, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	res, err := a.articleRepos.GetArticleByTitle(ctx, title)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (a *articleUseCase) GetTagAnalysisByDate(c context.Context, tagName string, date string) (*models.TagAnalysis, error) {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	res, err := a.articleRepos.GetTagAnalysisByDate(ctx, tagName, date)
	if err != nil {
		return nil, err
	}

	return res, nil
}
