package repository

import (
	"context"
	"database/sql"
	article "fairfax-media-technical-test/article"
	models "fairfax-media-technical-test/models"
	"fmt"

	"github.com/sirupsen/logrus"
)

type postgresArticleRepository struct {
	Conn *sql.DB
}

/*
PostgresArticleRepository ..
*/
func PostgresArticleRepository(Conn *sql.DB) article.ArticleRepository {

	return &postgresArticleRepository{Conn}
}
func (m *postgresArticleRepository) GetByID(ctx context.Context, id int64) (*models.Article, error) {

	query := `SELECT * FROM "fairfax-media".article WHERE ID = $1`
	list, err := m.fetch(ctx, query, id)
	if err != nil {
		return nil, err
	}
	a := &models.Article{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, models.NOT_FOUND_ERROR
	}

	a.Tags, err = m.getTagsByArticleID(ctx, id)

	return a, nil
}

func (m *postgresArticleRepository) fetch(ctx context.Context, query string, args ...interface{}) ([]*models.Article, error) {

	rows, err := m.Conn.QueryContext(ctx, query, args...)

	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	defer rows.Close()
	result := make([]*models.Article, 0)
	for rows.Next() {
		t := new(models.Article)
		err = rows.Scan(
			&t.ID,
			&t.Title,
			&t.Body,
			&t.Date,
		)

		if err != nil {
			logrus.Error(err)
			return nil, err
		}

		result = append(result, t)
	}
	return result, nil
}

func (m *postgresArticleRepository) getTagsByArticleID(ctx context.Context, args ...interface{}) ([]string, error) {

	query := `SELECT tg.tag FROM "fairfax-media".tag tg INNER JOIN "fairfax-media".article_tag_associations ats
    			ON tg.id = ats.tagid
				WHERE articleid = $1`
	rows, err := m.Conn.QueryContext(ctx, query, args...)

	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	defer rows.Close()
	result := make([]string, 0)
	for rows.Next() {
		t := new(string)
		err = rows.Scan(&t)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}

		result = append(result, *t)
	}
	return result, nil

}

func (m *postgresArticleRepository) GetArticleByTitle(ctx context.Context, title string) (*models.Article, error) {
	query := `SELECT * FROM "fairfax-media".article WHERE title = $1 `

	list, err := m.fetch(ctx, query, title)
	if err != nil {
		return nil, err
	}

	a := &models.Article{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, models.NOT_FOUND_ERROR
	}
	return a, nil
}

func (m *postgresArticleRepository) SaveArticle(ctx context.Context, a *models.Article) (int64, error) {

	query := `INSERT INTO "fairfax-media".article (id, title, body, date) VALUES
	($1, $2, $3, $4) ;`

	stmt, err := m.Conn.PrepareContext(ctx, query)
	if err != nil {

		return 0, err
	}

	res, err := stmt.ExecContext(ctx, a.ID, a.Title, a.Body, a.Date)
	if err != nil {

		return 0, err
	}

	m.SaveTags(ctx, a.Tags)

	logrus.Info(res)

	tagIds, err := m.getTagIdsByTagName(ctx, a.Tags)
	m.SaveArticleTagsAssosiations(ctx, a.ID, tagIds)
	inserted, err := m.GetByID(ctx, a.ID)
	return inserted.ID, err
}

func (m *postgresArticleRepository) SaveTags(ctx context.Context, a []string) (int64, error) {

	str := `INSERT INTO "fairfax-media".tag (tag) VALUES`
	var lastInsertID int64
	var err error
	for _, row := range a {
		fmt.Println(fmt.Sprintf("%s ('%s'); ", str, row))
		err = m.Conn.QueryRow(fmt.Sprintf("%s ('%s'); ", str, row)).Scan(&lastInsertID)
	}
	if err != nil {
		return 0, err
	}
	return lastInsertID, err
}

func (m *postgresArticleRepository) SaveArticleTagsAssosiations(ctx context.Context, articleID int64, tagIds []int64) (int64, error) {

	query := `INSERT INTO "fairfax-media".article_tag_associations (articleid, tagid) VALUES `

	var vals = []interface{}{}
	var ct = 2
	for _, row := range tagIds {
		query += fmt.Sprintf("($%d,$%d),", ct-1, ct)
		vals = append(vals, articleID, row)
		ct += 2
	}
	query = query[0 : len(query)-1]
	stmt, err := m.Conn.PrepareContext(ctx, query)
	if err != nil {

		return 0, err
	}

	res, err := stmt.ExecContext(ctx, vals...)
	if err != nil {

		return 0, err
	}

	return res.RowsAffected()
}

func (m *postgresArticleRepository) getTagIdsByTagName(ctx context.Context, tags []string) ([]int64, error) {

	query := `SELECT id FROM "fairfax-media".tag WHERE tag.tag in (`

	var vals = []interface{}{}

	for index, row := range tags {
		query += fmt.Sprintf("$%d,", index+1)
		vals = append(vals, row)
	}
	query = query[0 : len(query)-1]
	query += ")"

	rows, err := m.Conn.QueryContext(ctx, query, vals...)

	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	defer rows.Close()
	result := make([]int64, 0)
	for rows.Next() {
		t := new(int64)
		err = rows.Scan(&t)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}

		result = append(result, *t)
	}
	return result, nil

}

func (m *postgresArticleRepository) GetTagAnalysisByDate(ctx context.Context, tagName string, date string) (*models.TagAnalysis, error) {

	tagAnalysis := new(models.TagAnalysis)
	count, err := m.getTagCountByDate(ctx, tagName, date)
	tagAnalysis.Tag = tagName
	tagAnalysis.Count = count
	res, err := m.getArticles(ctx, tagName, date)
	tagAnalysis.Articles = res
	restring, err := m.getRelatedTags(ctx, tagName, date)
	tagAnalysis.RelatedTags = restring
	// query := `SELECT * FROM "fairfax-media".article WHERE ID = $1`
	// list, err := m.fetch(ctx, query, id)
	// if err != nil {
	// 	return nil, err
	// }
	// a := &models.Article{}
	// if len(list) > 0 {
	// 	a = list[0]
	// } else {
	// 	return nil, models.NOT_FOUND_ERROR
	// }

	// a.Tags, err = m.getTagsByArticleID(ctx, id)

	return tagAnalysis, err
}

func (m *postgresArticleRepository) getTagCountByDate(ctx context.Context, args ...interface{}) (int64, error) {

	query := `SELECT count(ar.id) FROM "fairfax-media".article ar
				INNER JOIN "fairfax-media".article_tag_associations ats
				  ON ar.id = ats.articleid INNER JOIN "fairfax-media".tag tg ON ats.tagid = tg.id
				   GROUP BY tag, ar.date
  					HAVING tg.tag = $1 AND ar.date = $2;`
	rows, err := m.Conn.QueryContext(ctx, query, args...)

	if err != nil {
		logrus.Error(err)
		return 0, err
	}
	defer rows.Close()
	result := int64(0)
	for rows.Next() {
		err = rows.Scan(&result)

		if err != nil {
			logrus.Error(err)
			return 0, err
		}
	}
	return result, nil
}

func (m *postgresArticleRepository) getArticles(ctx context.Context, args ...interface{}) ([]int64, error) {

	query := `SELECT ar.id FROM "fairfax-media".article ar
					INNER JOIN "fairfax-media".article_tag_associations ats
					ON ar.id = ats.articleid INNER JOIN "fairfax-media".tag tg ON ats.tagid = tg.id
				WHERE tg.tag = $1 AND ar.date = $2
				ORDER BY ar.id;`

	rows, err := m.Conn.QueryContext(ctx, query, args...)

	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	defer rows.Close()
	result := make([]int64, 0)
	for rows.Next() {
		t := new(int64)
		err = rows.Scan(&t)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}

		result = append(result, *t)
	}
	return result, nil

}

func (m *postgresArticleRepository) getRelatedTags(ctx context.Context, args ...interface{}) ([]string, error) {

	query := `SELECT DISTINCT(tg.tag) FROM "fairfax-media".article ar
				INNER JOIN "fairfax-media".article_tag_associations ats
				ON ar.id = ats.articleid INNER JOIN "fairfax-media".tag tg ON ats.tagid = tg.id
			WHERE ar.id in (
				SELECT ar.id FROM "fairfax-media".article ar
				INNER JOIN "fairfax-media".article_tag_associations ats
				ON ar.id = ats.articleid INNER JOIN "fairfax-media".tag tg ON ats.tagid = tg.id
			WHERE tg.tag = $1 AND ar.date = $2
			) AND tg.tag != $1;`

	rows, err := m.Conn.QueryContext(ctx, query, args...)

	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	defer rows.Close()
	result := make([]string, 0)
	for rows.Next() {
		t := new(string)
		err = rows.Scan(&t)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}

		result = append(result, *t)
	}
	return result, nil

}
