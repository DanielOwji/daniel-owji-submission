package article

import (
	"context"
	models "fairfax-media-technical-test/models"
)

type ArticleUseCase interface {
	GetByID(ctx context.Context, id int64) (*models.Article, error)
	SaveArticle(context.Context, *models.Article) (*models.Article, error)
	GetTagAnalysisByDate(ctx context.Context, tagName string, date string) (*models.TagAnalysis, error)
}
