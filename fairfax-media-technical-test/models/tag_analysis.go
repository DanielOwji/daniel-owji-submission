package models

type TagAnalysis struct {
	Tag         string   `json:"tag"`
	Count       int64    `json:"count"`
	Articles    []int64  `json:"articles"`
	RelatedTags []string `json:"related_tags"`
}
